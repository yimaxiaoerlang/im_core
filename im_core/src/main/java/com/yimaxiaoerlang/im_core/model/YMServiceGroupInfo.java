package com.yimaxiaoerlang.im_core.model;

public class YMServiceGroupInfo {
    private String gid;
    private String groupAvatar;
    private String groupName;
    private int groupType; //0单聊 1群聊 2聊天室
    private int isTop;
    private int notReadCnt;

    public YMServiceGroupInfo(String gid, String groupAvatar, String groupName, int groupType, int isTop, int notReadCnt) {
        this.gid = gid;
        this.groupAvatar = groupAvatar;
        this.groupName = groupName;
        this.groupType = groupType;
        this.isTop = isTop;
        this.notReadCnt = notReadCnt;
    }

    public YMServiceGroupInfo() {
    }

    public String getGid() {
        return gid;
    }

    public void setGid(String gid) {
        this.gid = gid;
    }

    public String getGroupAvatar() {
        return groupAvatar;
    }

    public void setGroupAvatar(String groupAvatar) {
        this.groupAvatar = groupAvatar;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public int getGroupType() {
        return groupType;
    }

    public void setGroupType(int groupType) {
        this.groupType = groupType;
    }

    public int getIsTop() {
        return isTop;
    }

    public void setIsTop(int isTop) {
        this.isTop = isTop;
    }

    public int getNotReadCnt() {
        return notReadCnt;
    }

    public void setNotReadCnt(int notReadCnt) {
        this.notReadCnt = notReadCnt;
    }

    @Override
    public String toString() {
        return "ServiceGroupInfo{" +
                "gid='" + gid + '\'' +
                ", groupAvatar='" + groupAvatar + '\'' +
                ", groupName='" + groupName + '\'' +
                ", groupType=" + groupType +
                ", isTop=" + isTop +
                ", notReadCnt=" + notReadCnt +
                '}';
    }
}
