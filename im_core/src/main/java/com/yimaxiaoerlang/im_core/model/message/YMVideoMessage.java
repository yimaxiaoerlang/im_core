package com.yimaxiaoerlang.im_core.model.message;

/**
 * 视频消息
 */
public class YMVideoMessage extends YMMessageContent {
    private String url;
    private String thumbnail;
    private String localPath;
    private String localThumbnail;
    private int duration;

    private int width;
    private int height;
    public static YMVideoMessage create(String url) {
        YMVideoMessage message = new YMVideoMessage();
        message.setUrl(url);
        return message;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getLocalPath() {
        return localPath;
    }

    public void setLocalPath(String localPath) {
        this.localPath = localPath;
    }

    public String getLocalThumbnail() {
        return localThumbnail;
    }

    public void setLocalThumbnail(String localThumbnail) {
        this.localThumbnail = localThumbnail;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getTime(){
        if (duration<60){
            if (duration<10){
                return  "00:0"+duration;
            }else{
                return  "00:"+duration;
            }
        }else {
            int number=duration/60;
            if (number<10){
                return "0"+number+":"+duration%60;
            }else{
                return ""+number+":"+duration%60;
            }
        }
    }


}

