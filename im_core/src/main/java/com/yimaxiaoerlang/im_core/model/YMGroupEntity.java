package com.yimaxiaoerlang.im_core.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class YMGroupEntity {

    private GroupBean group;
    private List<UserListBean> userList;
    private List<Object> messageList;

    public GroupBean getGroup() {
        return group;
    }

    public void setGroup(GroupBean group) {
        this.group = group;
    }

    public List<UserListBean> getUserList() {
        return userList;
    }

    public void setUserList(List<UserListBean> userList) {
        this.userList = userList;
    }

    public List<Object> getMessageList() {
        return messageList;
    }

    public void setMessageList(List<Object> messageList) {
        this.messageList = messageList;
    }

    public static class GroupBean {
        @SerializedName("gid")
        private String groupId;
        private String groupName;
        private String groupAvatar;
        private Integer groupType;
        private Integer notReadCnt;
        private Integer isTop;
        private Integer isDisturb;

        public String getGroupId() {
            return groupId;
        }

        public void setGroupId(String groupId) {
            this.groupId = groupId;
        }

        public String getGroupName() {
            return groupName;
        }

        public void setGroupName(String groupName) {
            this.groupName = groupName;
        }

        public String getGroupAvatar() {
            return groupAvatar;
        }

        public void setGroupAvatar(String groupAvatar) {
            this.groupAvatar = groupAvatar;
        }

        public Integer getGroupType() {
            return groupType;
        }

        public void setGroupType(Integer groupType) {
            this.groupType = groupType;
        }

        public Integer getNotReadCnt() {
            return notReadCnt;
        }

        public void setNotReadCnt(Integer notReadCnt) {
            this.notReadCnt = notReadCnt;
        }

        public Integer getIsTop() {
            return isTop;
        }

        public void setIsTop(Integer isTop) {
            this.isTop = isTop;
        }

        public Integer getIsDisturb() {
            return isDisturb;
        }

        public void setIsDisturb(Integer isDisturb) {
            this.isDisturb = isDisturb;
        }
    }

    public static class UserListBean {
        @SerializedName("gid")
        private String groupId;
        @SerializedName("uid")
        private String userId;
        private Integer userType;
        private String username;
        private String userAvatar;
        private Integer gender;
        private Integer isDisturb;

        public String getGroupId() {
            return groupId;
        }

        public void setGroupId(String groupId) {
            this.groupId = groupId;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public Integer getUserType() {
            return userType;
        }

        public void setUserType(Integer userType) {
            this.userType = userType;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getUserAvatar() {
            return userAvatar;
        }

        public void setUserAvatar(String userAvatar) {
            this.userAvatar = userAvatar;
        }

        public Integer getGender() {
            return gender;
        }

        public void setGender(Integer gender) {
            this.gender = gender;
        }

        public Integer getIsDisturb() {
            return isDisturb;
        }

        public void setIsDisturb(Integer isDisturb) {
            this.isDisturb = isDisturb;
        }
    }
}
