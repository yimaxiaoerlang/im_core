package com.yimaxiaoerlang.im_core.model.message;

/**
 * 时间消息
 */
public class YMTimeMessage extends YMMessageContent {
    private String time;

    public static YMTimeMessage create(String time) {
        YMTimeMessage message = new YMTimeMessage();
        message.setTime(time);
        return message;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }


}

