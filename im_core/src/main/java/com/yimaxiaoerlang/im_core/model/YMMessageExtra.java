package com.yimaxiaoerlang.im_core.model;

import java.io.Serializable;

public class YMMessageExtra implements Serializable {
    private int width;
    private int height;
    private int duration;
    private String imageUrl;

    public YMMessageExtra(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public YMMessageExtra(int width, int height, int duration, String imageUrl) {
        this.width = width;
        this.height = height;
        this.duration = duration;
        this.imageUrl = imageUrl;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }


}


