package com.yimaxiaoerlang.im_core.model.message;

/**
 * 图片消息
 */
public class YMFileMessage extends YMMessageContent {
    private String url;

    public static YMFileMessage create(String url) {
        YMFileMessage message = new YMFileMessage();
        message.setUrl(url);
        return message;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }


}

