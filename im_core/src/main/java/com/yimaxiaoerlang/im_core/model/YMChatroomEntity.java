package com.yimaxiaoerlang.im_core.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class YMChatroomEntity {

    private ChatRoomBean chatRoom;
    private List<UserListBean> userList;

    public ChatRoomBean getChatRoom() {
        return chatRoom;
    }

    public void setChatRoom(ChatRoomBean chatRoom) {
        this.chatRoom = chatRoom;
    }

    public List<UserListBean> getUserList() {
        return userList;
    }

    public void setUserList(List<UserListBean> userList) {
        this.userList = userList;
    }

    public static class ChatRoomBean {
        private Integer createBy;
        private String createTime;
        private Integer updateBy;
        private String updateTime;
        private Boolean isDelete;
        private Integer version;
        private String id;
        @SerializedName("gid")
        private String groupId;
        private String groupName;
        private String groupAvatar;
        private Integer groupType;
        private Object label;
        private Integer tenantId;

        public Integer getCreateBy() {
            return createBy;
        }

        public void setCreateBy(Integer createBy) {
            this.createBy = createBy;
        }

        public String getCreateTime() {
            return createTime;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public Integer getUpdateBy() {
            return updateBy;
        }

        public void setUpdateBy(Integer updateBy) {
            this.updateBy = updateBy;
        }

        public String getUpdateTime() {
            return updateTime;
        }

        public void setUpdateTime(String updateTime) {
            this.updateTime = updateTime;
        }

        public Boolean getIsDelete() {
            return isDelete;
        }

        public void setIsDelete(Boolean isDelete) {
            this.isDelete = isDelete;
        }

        public Integer getVersion() {
            return version;
        }

        public void setVersion(Integer version) {
            this.version = version;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getGroupId() {
            return groupId;
        }

        public void setGroupId(String groupId) {
            this.groupId = groupId;
        }

        public String getGroupName() {
            return groupName;
        }

        public void setGroupName(String groupName) {
            this.groupName = groupName;
        }

        public String getGroupAvatar() {
            return groupAvatar;
        }

        public void setGroupAvatar(String groupAvatar) {
            this.groupAvatar = groupAvatar;
        }

        public Integer getGroupType() {
            return groupType;
        }

        public void setGroupType(Integer groupType) {
            this.groupType = groupType;
        }

        public Object getLabel() {
            return label;
        }

        public void setLabel(Object label) {
            this.label = label;
        }

        public Integer getTenantId() {
            return tenantId;
        }

        public void setTenantId(Integer tenantId) {
            this.tenantId = tenantId;
        }
    }

    public static class UserListBean {
        @SerializedName("gid")
        private String groupId;
        @SerializedName("uid")
        private String userId;
        private Object noteName;
        private Integer userType;
        private String username;
        private String userAvatar;
        private Integer age;
        private Integer gender;
        private Integer isDisturb;
        private Integer isBan;

        public String getGroupId() {
            return groupId;
        }

        public void setGroupId(String groupId) {
            this.groupId = groupId;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public Object getNoteName() {
            return noteName;
        }

        public void setNoteName(Object noteName) {
            this.noteName = noteName;
        }

        public Integer getUserType() {
            return userType;
        }

        public void setUserType(Integer userType) {
            this.userType = userType;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getUserAvatar() {
            return userAvatar;
        }

        public void setUserAvatar(String userAvatar) {
            this.userAvatar = userAvatar;
        }

        public Integer getAge() {
            return age;
        }

        public void setAge(Integer age) {
            this.age = age;
        }

        public Integer getGender() {
            return gender;
        }

        public void setGender(Integer gender) {
            this.gender = gender;
        }

        public Integer getIsDisturb() {
            return isDisturb;
        }

        public void setIsDisturb(Integer isDisturb) {
            this.isDisturb = isDisturb;
        }

        public Integer getIsBan() {
            return isBan;
        }

        public void setIsBan(Integer isBan) {
            this.isBan = isBan;
        }
    }
}
