package com.yimaxiaoerlang.im_core.model.message;

import androidx.annotation.NonNull;

/**
 * 语音消息
 */
public class YMVoiceMessage extends YMMessageContent {
    private String url;
    private int duration;
    private String localPath;
    public YMVoiceMessage() {
    }

    @NonNull
    public static YMVoiceMessage create(String url) {
        YMVoiceMessage message = new YMVoiceMessage();
        message.setUrl(url);
        return message;
    }


    public YMVoiceMessage(int duration, String localPath) {
        this.duration = duration;
        this.localPath = localPath;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }


    public String getLocalPath() {
        return localPath;
    }

    public void setLocalPath(String localPath) {
        this.localPath = localPath;
    }
}

