package com.yimaxiaoerlang.im_core.model;

import com.yimaxiaoerlang.http.GsonUtils;
import com.yimaxiaoerlang.im_core.model.message.YMCustomMessage;
import com.yimaxiaoerlang.im_core.model.message.YMFileMessage;
import com.yimaxiaoerlang.im_core.model.message.YMImageMessage;
import com.yimaxiaoerlang.im_core.model.message.YMMessage;
import com.yimaxiaoerlang.im_core.model.message.YMMessageContent;
import com.yimaxiaoerlang.im_core.model.message.YMSystemMessage;
import com.yimaxiaoerlang.im_core.model.message.YMTextMessage;
import com.yimaxiaoerlang.im_core.model.message.YMTimeMessage;
import com.yimaxiaoerlang.im_core.model.message.YMVideoMessage;
import com.yimaxiaoerlang.im_core.model.message.YMVoiceMessage;

public class YMServiceMessageInfo {
    private String id;
    private String gid;
    private String mid;
    private String msg;
    private String msgTime;
    private int msgState;
    private int msgType;// 0:系统消息 1：聊天消息
    private int offline;
    private int subType;  // 101: 文本消息 102:音频 103:视频 104：图片 105：文件
    private String tid;
    private String uid;
    private String userAvatar;
    private String username;
    private Object extraData;

    public YMServiceMessageInfo() {
    }

    public YMServiceMessageInfo(String id, String gid, String mid, String msg, String msgTime, int msgState, int msgType, int offline, int subType, String tid, String uid, String userAvatar, String username, Object extraData) {
        this.id = id;
        this.gid = gid;
        this.mid = mid;
        this.msg = msg;
        this.msgTime = msgTime;
        this.msgState = msgState;
        this.msgType = msgType;
        this.offline = offline;
        this.subType = subType;
        this.tid = tid;
        this.uid = uid;
        this.userAvatar = userAvatar;
        this.username = username;
        this.extraData = extraData;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGid() {
        return gid;
    }

    public void setGid(String gid) {
        this.gid = gid;
    }

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getMsgTime() {
        return msgTime;
    }

    public void setMsgTime(String msgTime) {
        this.msgTime = msgTime;
    }

    public int getMsgState() {
        return msgState;
    }

    public void setMsgState(int msgState) {
        this.msgState = msgState;
    }

    public int getMsgType() {
        return msgType;
    }

    public void setMsgType(int msgType) {
        this.msgType = msgType;
    }

    public int getOffline() {
        return offline;
    }

    public void setOffline(int offline) {
        this.offline = offline;
    }

    public int getSubType() {
        return subType;
    }

    public void setSubType(int subType) {
        this.subType = subType;
    }

    public String getTid() {
        return tid;
    }

    public void setTid(String tid) {
        this.tid = tid;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getUserAvatar() {
        return userAvatar;
    }

    public void setUserAvatar(String userAvatar) {
        this.userAvatar = userAvatar;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Object getExtraData() {
        return extraData;
    }

    public void setExtraData(Object extraData) {
        this.extraData = extraData;
    }

    @Override
    public String toString() {
        return "ServiceMessageInfo{" +
                "gid='" + gid + '\'' +
                ", mid=" + mid +
                ", mdid='" + msg + '\'' +
                ", msgTime='" + msgTime + '\'' +
                ", msgState=" + msgState +
                ", msgType=" + msgType +
                ", offline=" + offline +
                ", subType=" + subType +
                ", tid='" + tid + '\'' +
                ", uid='" + uid + '\'' +
                ", userAvatar='" + userAvatar + '\'' +
                ", username='" + username + '\'' +
                ", extraData=" + extraData +
                '}';
    }

    public YMMessage toMessage(String ueserId) {
        YMMessage message = new YMMessage();
        message.setMessageId(id);
        message.setMessageDirection(ueserId.equals(uid) ? YMMessage.MessageDirection.SEND : YMMessage.MessageDirection.RECEIVE);
        message.setTargetId(gid);
        message.setContent(getMessgae());
        message.setSendUserName(username);
        message.setSendUserIcon(userAvatar);
        message.setSenderUserId(uid);
        message.setServiceJson(GsonUtils.toJson(this));

        if (extraData != null) {
            message.setExtra(extraData.toString());
        }
        return message;
    }

    public YMMessageContent getMessgae() {
        YMMessageContent messageContent = null;
        YMMessageExtra messageExtra = null;
        if (extraData != null && getSubType() != 203 && getSubType() != 207 && getSubType() != 208 && getSubType() != 209 && getSubType() != 210) {

            messageExtra = GsonUtils.toBean(extraData.toString(), YMMessageExtra.class);
        }

        switch (getSubType()) {
            case 3://时间
                messageContent = YMTimeMessage.create(getMsg());
                break;
            case 4: // 系统消息
                String message = "";
                if (msg.equals("您邀请")) {
                    String[] strings = msg.split("您邀请");
                    if (strings.length > 0) {
                        message = strings[0];
                    } else {
                        message = msg;
                    }
                } else {
                    message = msg;
                }
                messageContent = YMSystemMessage.create(message);
                break;
            case 203://通话相关
            case 207://通话相关
            case 208://通话相关
            case 209://通话相关
            case 210://通话相关
            case 101://文字
                messageContent = YMTextMessage.create(getMsg());
                break;
            case 102://语音
                YMVoiceMessage voiceMessage = YMVoiceMessage.create(getMsg());
//                MessageExtra voiceExtra = GsonUtils.toBean(extraData.toString(), MessageExtra.class);
//                voiceMessage.setDuration(voiceExtra.getDuration());
                if (messageExtra != null) {
                    voiceMessage.setDuration(messageExtra.getDuration());
                }
                messageContent = voiceMessage;
                break;
            case 103://视频
                YMVideoMessage videoMessage = YMVideoMessage.create(getMsg());
                if (messageExtra != null) {
                    videoMessage.setDuration(messageExtra.getDuration());
                    videoMessage.setThumbnail(messageExtra.getImageUrl());
                }
                messageContent = videoMessage;
                break;
            case 104://图片
//                messageContent = TextMessage.create("[图片]");
                YMImageMessage imageMessage = YMImageMessage.create(getMsg());
                if (messageExtra != null) {
                    imageMessage.setHeight(messageExtra.getHeight());
                    imageMessage.setWidth(messageExtra.getWidth());
                }
                messageContent = imageMessage;
                break;
            case 105://文件
//                messageContent = TextMessage.create("[文件]");
                messageContent = YMFileMessage.create(getMsg());
                break;
            case 100:
                messageContent = YMCustomMessage.create(getMsg());
                break;
            default://默认
                messageContent = YMTextMessage.create(getMsg());
                break;
        }

        messageContent.setMessageTime(getMsgTime());
        if (messageExtra != null) {
            messageContent.setExtra(messageExtra);
        }
        return messageContent;

    }

}
