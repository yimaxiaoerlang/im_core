package com.yimaxiaoerlang.im_core.model;

import com.yimaxiaoerlang.im_core.model.conversation.YMConversation;
import com.yimaxiaoerlang.im_core.model.message.YMMessageContent;

import java.util.ArrayList;
import java.util.List;

public class YMServiceConversation {
    private YMServiceGroupInfo group;
    private List<YMSocketUser> userList;
    private List<YMServiceMessageInfo> messageList;
//    private String targetId;
//    private String targetName;
//    private String targetPortrait;


    public YMServiceConversation() {
    }

    public YMServiceConversation(YMServiceGroupInfo group, List<YMSocketUser> userList, List<YMServiceMessageInfo> messageList) {
        this.group = group;
        this.userList = userList;
        this.messageList = messageList;
    }

    public YMServiceGroupInfo getGroup() {
        return group;
    }

    public void setGroup(YMServiceGroupInfo group) {
        this.group = group;
    }

    public List<YMSocketUser> getUserList() {
        return userList;
    }

    public void setUserList(List<YMSocketUser> userList) {
        this.userList = userList;
    }

    public List<YMServiceMessageInfo> getMessageList() {
        return messageList;
    }

    public void setMessageList(List<YMServiceMessageInfo> messageList) {
        this.messageList = messageList;
    }

    @Override
    public String toString() {
        return "ServiceConversation{" +
                "group=" + group +
                ", userList=" + userList +
                ", messageList=" + messageList +
                '}';
    }

    /**
     * 把服务端返回的消息数据转换成本地的数据模型
     *
     * @return
     */
    public YMConversation toConversation() {
        YMConversation conversation = new YMConversation();
        int type = group.getGroupType();
        //未读消息数量
        conversation.setUnreadMessageCount(group.getNotReadCnt());
        conversation.setLatestMessage(getMessgae());
        conversation.setGroupId(group.getGid());
        StringBuffer sb = new StringBuffer();
        if (userList != null && userList.size() > 0) {
            ArrayList<YMPersonnel> personnels = new ArrayList<>();
            for (YMSocketUser socketUser : userList) {
                if (socketUser != null) {
                    personnels.add(new YMPersonnel(socketUser.getUid(), socketUser.getUsername(), socketUser.getUserAvatar()));
                }
            }
            if (type == 1 || type ==2) for (int i = 0; i < userList.size(); i++) {
                if (i > 0) sb.append(",");
                sb.append(userList.get(i).getUserAvatar());
            }
            conversation.setPersonnels(personnels);
        }

        switch (type) {//0单聊 1群聊 2聊天室
            case 0://单聊
                conversation.setConversationType(YMConversation.ConversationType.PRIVATE);
                conversation.setConversationTitle(userList.get(0).getUsername());
                conversation.setPortraitUrl(userList.get(0).getUserAvatar());
                conversation.setTargetId(userList.get(0).getUid());
                break;
            case 1://群聊
                conversation.setConversationType(YMConversation.ConversationType.GROUP);
                conversation.setConversationTitle(group.getGroupName());
                conversation.setPortraitUrl(sb.toString());
                conversation.setTargetId(group.getGid());
                break;
            case 2://聊天室
                conversation.setConversationType(YMConversation.ConversationType.CHATROOM);
                conversation.setConversationTitle(group.getGroupName());
                conversation.setPortraitUrl(sb.toString());
                conversation.setTargetId(group.getGid());
                break;
        }


        return conversation;
    }

    //    MessageType_TEXT = 101, //文本
//    MessageType_AUDIO = 102, //音频
//    MessageType_VIDEO = 103, //视频
//    MessageType_PICTURE = 104, //图片
//    MessageType_FILE = 105, //文件
    private YMMessageContent getMessgae() {
        YMMessageContent messageContent = null;
        if (messageList == null || messageList.isEmpty()) {
            return null;
        }

        YMServiceMessageInfo serviceMessage = messageList.get(0);
        return serviceMessage.getMessgae();


    }

}
