package com.yimaxiaoerlang.im_core.model.message;

/**
 * 自定义消息
 */
public class YMCustomMessage extends YMMessageContent {
    private String content;

    public static YMCustomMessage create(String content) {
        YMCustomMessage message = new YMCustomMessage();
        message.setContent(content);
        return message;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
