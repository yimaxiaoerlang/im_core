package com.yimaxiaoerlang.im_core.model.message;

/**
 * 文本消息
 */
public class YMSystemMessage extends YMMessageContent {
    private String content;

    public static YMSystemMessage create(String text) {
        YMSystemMessage message = new YMSystemMessage();
        message.setContent(text);
        return message;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }


}
