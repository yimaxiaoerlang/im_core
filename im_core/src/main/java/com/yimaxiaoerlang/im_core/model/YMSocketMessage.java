package com.yimaxiaoerlang.im_core.model;

import android.util.Log;

import com.yimaxiaoerlang.http.GsonUtils;
import com.yimaxiaoerlang.im_core.model.message.YMCustomMessage;
import com.yimaxiaoerlang.im_core.model.message.YMFileMessage;
import com.yimaxiaoerlang.im_core.model.message.YMImageMessage;
import com.yimaxiaoerlang.im_core.model.message.YMMessage;
import com.yimaxiaoerlang.im_core.model.message.YMMessageContent;
import com.yimaxiaoerlang.im_core.model.message.YMSystemMessage;
import com.yimaxiaoerlang.im_core.model.message.YMTextMessage;
import com.yimaxiaoerlang.im_core.model.message.YMTimeMessage;
import com.yimaxiaoerlang.im_core.model.message.YMVideoMessage;
import com.yimaxiaoerlang.im_core.model.message.YMVoiceMessage;

public class YMSocketMessage {
    private String gid;
    private String uid;
    private String tid;
    private YMSocketUser fromUser;
    private YMSocketUser tarUser;
    private YMSocketMessageContent message;

    public YMSocketMessage(String gid, String uid, String tid, YMSocketUser fromUser, YMSocketUser tarUser, YMSocketMessageContent message) {
        this.gid = gid;
        this.uid = uid;
        this.tid = tid;
        this.fromUser = fromUser;
        this.tarUser = tarUser;
        this.message = message;
    }

    public YMSocketMessage() {
    }

    public String getGid() {
        return gid;
    }

    public void setGid(String gid) {
        this.gid = gid;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getTid() {
        return tid;
    }

    public void setTid(String tid) {
        this.tid = tid;
    }

    public YMSocketUser getFromUser() {
        return fromUser;
    }

    public void setFromUser(YMSocketUser fromUser) {
        this.fromUser = fromUser;
    }

    public YMSocketUser getTarUser() {
        return tarUser;
    }

    public void setTarUser(YMSocketUser tarUser) {
        this.tarUser = tarUser;
    }

    public YMSocketMessageContent getMessage() {
        return message;
    }

    public void setMessage(YMSocketMessageContent message) {
        this.message = message;
    }

    public YMMessage toLocalMessage(String locaUserId) {

        if (message == null) return null;
        if (locaUserId == null || locaUserId.isEmpty()) {
            Log.e("SocketMessage", "locaUserId为空无法转换消息");
            return null;
        }
        if (fromUser == null) {
            Log.e("SocketMessage", "fromUser为空无法转换消息");
            return null;
        }
        YMMessage data = new YMMessage();
        //判断消息类型
        YMMessageContent content = getLocalMessageContent();
        data.setContent(content);
        data.setTargetId(gid);
        data.setSendUserName(fromUser.getUsername());
        data.setSendUserIcon(fromUser.getUserAvatar());
        data.setMessageId(message.getMsgId());
        data.setReadState(YMMessage.MessageReadState.UNREAD);
        data.setSenderUserId(uid);
        data.setSocketJson(GsonUtils.toJson(this));
        if (getMessage().getExtraData() != null) {
            data.setExtra(getMessage().getExtraData().toString());
        }
        if (fromUser != null) {
            data.setMessageDirection(locaUserId.equals(uid) ? YMMessage.MessageDirection.SEND : YMMessage.MessageDirection.RECEIVE);
        } else {
            data.setMessageDirection(YMMessage.MessageDirection.RECEIVE);
        }
        return data;
    }

    private YMMessageContent getLocalMessageContent() {
        YMMessageContent messageContent = null;
        YMMessageExtra messageExtra = null;
        if (message.getExtraData() != null) {
            messageExtra = GsonUtils.toBean(GsonUtils.toJson(message.getExtraData()), YMMessageExtra.class);
        }
        switch (message.getMsgType()) {
            case 3://时间
                messageContent = YMTimeMessage.create(message.getMsg());
                break;
            case 4: // 系统消息
                String message_ = "";
                if (message.getMsg().equals("您邀请")) {
                    String[] strings = message.getMsg().split("您邀请");
                    if (strings.length > 0) {
                        message_ = strings[0];
                    } else {
                        message_ = message.getMsg();
                    }
                } else {
                    message_ = message.getMsg();
                }
                messageContent = YMSystemMessage.create(message_);
                break;
            case 100:// 自定义消息
                messageContent = YMCustomMessage.create(message.getMsg());
                break;
            case 101://文字
                messageContent = YMTextMessage.create(message.getMsg());
                break;
            case 102://语音
                YMVoiceMessage voiceMessage = YMVoiceMessage.create(message.getMsg());

//                MessageExtra voiceExtra = GsonUtils.toBean(getMessage().getExtraData().toString(), MessageExtra.class);
                if (messageExtra != null) {
                    voiceMessage.setDuration(messageExtra.getDuration());
                }
                messageContent = voiceMessage;
                break;
            case 103://视频
                YMVideoMessage videoMessage = YMVideoMessage.create(message.getMsg());
                if (messageExtra != null) {
                    videoMessage.setDuration(messageExtra.getDuration());
                    videoMessage.setThumbnail(messageExtra.getImageUrl());
                    videoMessage.setWidth(messageExtra.getWidth());
                    videoMessage.setHeight(messageExtra.getHeight());
                }
//                MessageExtra videoExtra = GsonUtils.toBean(GsonUtils.toJson(message.getExtraData()),MessageExtra.class);

                messageContent = videoMessage;

                break;
            case 104://图片
//                messageContent = TextMessage.create("[图片]");
                YMImageMessage imageMessage = YMImageMessage.create(message.getMsg());
                if (messageExtra != null) {
                    imageMessage.setWidth(messageExtra.getWidth());
                    imageMessage.setHeight(messageExtra.getHeight());
                }
                messageContent = imageMessage;
                break;
            case 105://文件
                messageContent = YMFileMessage.create(message.getMsg());
                break;
            default:
                messageContent = YMTextMessage.create(message.getMsg());
                break;
        }
        messageContent.setMessageTime(message.getMsgTime());
        if (messageExtra != null) {
            messageContent.setExtra(messageExtra);
        }
        return messageContent;

    }
}
