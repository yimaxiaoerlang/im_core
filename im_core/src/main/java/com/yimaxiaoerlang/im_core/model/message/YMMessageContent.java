package com.yimaxiaoerlang.im_core.model.message;

import com.yimaxiaoerlang.im_core.model.YMMessageExtra;

import java.io.Serializable;

public abstract class YMMessageContent implements Serializable {
    private static final String TAG = "MessageContent";
    protected YMMessageExtra extra;
    private String messageTime;

    public String getMessageTime() {
        return messageTime;
    }

    public void setMessageTime(String messageTime) {
        this.messageTime = messageTime;
    }

    public YMMessageExtra getExtra() {
        return extra;
    }

    public void setExtra(YMMessageExtra extra) {
        this.extra = extra;
    }
}
