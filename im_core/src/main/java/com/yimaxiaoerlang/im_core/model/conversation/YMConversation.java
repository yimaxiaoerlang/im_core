package com.yimaxiaoerlang.im_core.model.conversation;

import com.yimaxiaoerlang.im_core.model.message.YMMessageContent;
import com.yimaxiaoerlang.im_core.model.YMPersonnel;

import java.io.Serializable;
import java.util.ArrayList;

public class YMConversation implements Serializable {
    private static final String TAG = YMConversation.class.getSimpleName();
    private YMConversation.ConversationType conversationType;
    private String targetId;
    private String channelId;
    private String conversationTitle;
    private String portraitUrl;
    private int unreadMessageCount;
    private YMMessageContent latestMessage;
    private String groupId;//补充运用判断消息的
    private ArrayList<YMPersonnel> personnels;

    public static String getTAG() {
        return TAG;
    }

    public ConversationType getConversationType() {
        return conversationType;
    }

    public void setConversationType(ConversationType conversationType) {
        this.conversationType = conversationType;
    }

    public String getTargetId() {
        return targetId;
    }

    public void setTargetId(String targetId) {
        this.targetId = targetId;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getConversationTitle() {
        return conversationTitle;
    }

    public void setConversationTitle(String conversationTitle) {
        this.conversationTitle = conversationTitle;
    }

    public String getPortraitUrl() {
        return portraitUrl;
    }

    public void setPortraitUrl(String portraitUrl) {
        this.portraitUrl = portraitUrl;
    }

    public int getUnreadMessageCount() {
        return unreadMessageCount;
    }

    public void setUnreadMessageCount(int unreadMessageCount) {
        this.unreadMessageCount = unreadMessageCount;
    }

    public YMMessageContent getLatestMessage() {
        return latestMessage;
    }

    public void setLatestMessage(YMMessageContent latestMessage) {
        this.latestMessage = latestMessage;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public ArrayList<YMPersonnel> getPersonnels() {
        return personnels;
    }

    public void setPersonnels(ArrayList<YMPersonnel> personnels) {
        this.personnels = personnels;
    }

    /**
     * 会话类型
     */
    public static enum ConversationType  implements Serializable {
        NONE(10, "none_type"),//没有
        PRIVATE(0, "private_type"),//私聊
        DISCUSSION(12, "discussion_type"),//讨论组
        GROUP(1, "group_type"),//群聊
        CHATROOM(2, "chatroom_type"),//聊天室
        CUSTOMER_SERVICE(15, "customer_service_type"),
        SYSTEM(16, "system_type"),
        APP_PUBLIC_SERVICE(17, "app_public_service_type"),
        PUBLIC_SERVICE(18, "public_service_type"),
        PUSH_SERVICE(19, "push_service_type"),
        ENCRYPTED(21, "encrypted_type"),
        RTC_ROOM(22, "rtc_room_type");

        private int value;
        private String name;

        private ConversationType(int value, String name) {
            this.value = value;
            this.name = name;
        }

        public int getValue() {
            return this.value;
        }

        public String getName() {
            return this.name;
        }

        public static YMConversation.ConversationType setValue(int code) {
            YMConversation.ConversationType[] var1 = values();
            int var2 = var1.length;

            for(int var3 = 0; var3 < var2; ++var3) {
                YMConversation.ConversationType c = var1[var3];
                if (code == c.getValue()) {
                    return c;
                }
            }

            return PRIVATE;
        }
    }
}
