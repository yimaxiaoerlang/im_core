package com.yimaxiaoerlang.im_core.model.message;

import com.yimaxiaoerlang.im_core.model.conversation.YMConversation.ConversationType;

public class YMMessage {
    private static final String TAG = "Message";
    private ConversationType conversationType;//会话类型
    private String targetId;//目标id
    private String channelId;//房间id
    private String messageId;//消息id
    private YMMessage.MessageDirection messageDirection;//消息的方向 收消息/发消息
    private String senderUserId;//发送用户的id
    //    private Message.ReceivedStatus receivedStatus;
//    private Message.SentStatus sentStatus;
    private long receivedTime;//收到时间
    private long sentTime;//发送时间
    private long readTime;//阅读时间
    //    private String objectName;
    private YMMessageContent content;//消息内容
    private String extra;//消息的附加内容
    //    private boolean canIncludeExpansion;
//    private boolean mayHasMoreMessagesBefore;
    private boolean isEmptyContent;//是不是空消息

    private MessageReadState readState;

    private String sendUserName;
    private String sendUserIcon;

    private String serviceJson;
    private String socketJson;

    public YMMessage() {

    }

    public YMMessage(String targetId, YMMessageContent content, String extra) {
        this.targetId = targetId;
        this.content = content;
        this.extra = extra;
    }

    public YMMessage(String targetId, YMMessageContent content) {
        this.targetId = targetId;
        this.content = content;
    }

    public ConversationType getConversationType() {
        return conversationType;
    }

    public void setConversationType(ConversationType conversationType) {
        this.conversationType = conversationType;
    }

    public String getTargetId() {
        return targetId;
    }

    public void setTargetId(String targetId) {
        this.targetId = targetId;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public MessageDirection getMessageDirection() {
        return messageDirection;
    }

    public void setMessageDirection(MessageDirection messageDirection) {
        this.messageDirection = messageDirection;
    }

    public String getSenderUserId() {
        return senderUserId;
    }

    public void setSenderUserId(String senderUserId) {
        this.senderUserId = senderUserId;
    }

    public long getReceivedTime() {
        return receivedTime;
    }

    public void setReceivedTime(long receivedTime) {
        this.receivedTime = receivedTime;
    }

    public long getSentTime() {
        return sentTime;
    }

    public void setSentTime(long sentTime) {
        this.sentTime = sentTime;
    }

    public long getReadTime() {
        return readTime;
    }

    public void setReadTime(long readTime) {
        this.readTime = readTime;
    }

    public YMMessageContent getContent() {
        return content;
    }

    public void setContent(YMMessageContent content) {
        this.content = content;
    }

    public String getExtra() {
        return extra;
    }

    public void setExtra(String extra) {
        this.extra = extra;
    }

    public boolean isEmptyContent() {
        return isEmptyContent;
    }

    public void setEmptyContent(boolean emptyContent) {
        isEmptyContent = emptyContent;
    }

    public MessageReadState getReadState() {
        return readState;
    }

    public void setReadState(MessageReadState readState) {
        this.readState = readState;
    }

    public String getSendUserName() {
        return sendUserName;
    }

    public void setSendUserName(String sendUserName) {
        this.sendUserName = sendUserName;
    }

    public String getSendUserIcon() {
        return sendUserIcon;
    }

    public void setSendUserIcon(String sendUserIcon) {
        this.sendUserIcon = sendUserIcon;
    }

    public String getServiceJson() {
        return serviceJson;
    }

    public void setServiceJson(String serviceJson) {
        this.serviceJson = serviceJson;
    }

    public String getSocketJson() {
        return socketJson;
    }

    public void setSocketJson(String socketJson) {
        this.socketJson = socketJson;
    }

    public static enum MessageReadState {
        READ(1),
        UNREAD(2);
        private int value;

        private MessageReadState(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    public static enum MessageDirection {
        SEND(1),
        RECEIVE(2);

        private int value;

        private MessageDirection(int value) {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }

        public static YMMessage.MessageDirection setValue(int code) {
            YMMessage.MessageDirection[] var1 = values();
            int var2 = var1.length;

            for (int var3 = 0; var3 < var2; ++var3) {
                YMMessage.MessageDirection c = var1[var3];
                if (code == c.getValue()) {
                    return c;
                }
            }

            return SEND;
        }
    }
}
