package com.yimaxiaoerlang.im_core.model;

public class YMSocketUser {
    private String gid;
    private String uid;
    private String noteName;
    private int userType;
    private String username;
    private String userAvatar;
    private String isDisturb;

    public YMSocketUser() {
    }

    public YMSocketUser(String gid, String uid, String noteName, int userType, String username, String userAvatar, String isDisturb) {
        this.gid = gid;
        this.uid = uid;
        this.noteName = noteName;
        this.userType = userType;
        this.username = username;
        this.userAvatar = userAvatar;
        this.isDisturb = isDisturb;
    }

    public String getGid() {
        return gid;
    }

    public void setGid(String gid) {
        this.gid = gid;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getNoteName() {
        return noteName;
    }

    public void setNoteName(String noteName) {
        this.noteName = noteName;
    }

    public int getUserType() {
        return userType;
    }

    public void setUserType(int userType) {
        this.userType = userType;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserAvatar() {
        return userAvatar;
    }

    public void setUserAvatar(String userAvatar) {
        this.userAvatar = userAvatar;
    }

    public String getIsDisturb() {
        return isDisturb;
    }

    public void setIsDisturb(String isDisturb) {
        this.isDisturb = isDisturb;
    }
}
