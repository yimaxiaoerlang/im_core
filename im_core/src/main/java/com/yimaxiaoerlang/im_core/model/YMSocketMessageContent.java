package com.yimaxiaoerlang.im_core.model;

public class YMSocketMessageContent {
    private String msgId;
    private String msg;
    private int msgType;
    private String msgTime;
    private Object extraData;

    public YMSocketMessageContent() {
    }

    public YMSocketMessageContent(String msgId, String msg, int msgType, String msgTime, Object extraData) {
        this.msgId = msgId;
        this.msg = msg;
        this.msgType = msgType;
        this.msgTime = msgTime;
        this.extraData = extraData;
    }

    public String getMsgId() {
        return msgId;
    }

    public void setMsgId(String msgId) {
        this.msgId = msgId;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getMsgType() {
        return msgType;
    }

    public void setMsgType(int msgType) {
        this.msgType = msgType;
    }

    public String getMsgTime() {
        return msgTime;
    }

    public void setMsgTime(String msgTime) {
        this.msgTime = msgTime;
    }

    public Object getExtraData() {
        return extraData;
    }

    public void setExtraData(Object extraData) {
        this.extraData = extraData;
    }
}
