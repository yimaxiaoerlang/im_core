package com.yimaxiaoerlang.im_core.model.message;

/**
 * 图片消息
 */
public class YMImageMessage extends YMMessageContent {
    private String url;
    private String localPath;
    private int width;
    private int height;

    public static YMImageMessage create(String url) {
        YMImageMessage message = new YMImageMessage();
        message.setUrl(url);
        return message;
    }

    public YMImageMessage() {
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getLocalPath() {
        return localPath;
    }

    public void setLocalPath(String localPath) {
        this.localPath = localPath;
    }

    public YMImageMessage(String localPath) {
        this.localPath = localPath;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }
}

