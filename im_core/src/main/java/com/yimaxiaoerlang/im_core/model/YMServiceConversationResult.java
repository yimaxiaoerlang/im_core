package com.yimaxiaoerlang.im_core.model;

import java.util.ArrayList;
import java.util.List;

public class YMServiceConversationResult<T> {

    private ArrayList<Object> orders;
    private int pages;
    private int current;
    private Boolean optimizeCountSql;
    private Object countId;
    private int size;
    private Boolean hitCount;
    private Object maxLimit;
    private int total;
    private Boolean searchCount;
    private List<T> records;

    public YMServiceConversationResult() {
    }

    public YMServiceConversationResult(ArrayList<Object> orders, int pages, int current, Boolean optimizeCountSql, Object countId, int size, Boolean hitCount, Object maxLimit, int total, Boolean searchCount, List<T> records) {
        this.orders = orders;
        this.pages = pages;
        this.current = current;
        this.optimizeCountSql = optimizeCountSql;
        this.countId = countId;
        this.size = size;
        this.hitCount = hitCount;
        this.maxLimit = maxLimit;
        this.total = total;
        this.searchCount = searchCount;
        this.records = records;
    }

    public ArrayList<Object> getOrders() {
        return orders;
    }

    public void setOrders(ArrayList<Object> orders) {
        this.orders = orders;
    }

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    public int getCurrent() {
        return current;
    }

    public void setCurrent(int current) {
        this.current = current;
    }

    public Boolean getOptimizeCountSql() {
        return optimizeCountSql;
    }

    public void setOptimizeCountSql(Boolean optimizeCountSql) {
        this.optimizeCountSql = optimizeCountSql;
    }

    public Object getCountId() {
        return countId;
    }

    public void setCountId(Object countId) {
        this.countId = countId;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public Boolean getHitCount() {
        return hitCount;
    }

    public void setHitCount(Boolean hitCount) {
        this.hitCount = hitCount;
    }

    public Object getMaxLimit() {
        return maxLimit;
    }

    public void setMaxLimit(Object maxLimit) {
        this.maxLimit = maxLimit;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public Boolean getSearchCount() {
        return searchCount;
    }

    public void setSearchCount(Boolean searchCount) {
        this.searchCount = searchCount;
    }

    public List<T> getRecords() {
        return records;
    }

    public void setRecords(List<T> records) {
        this.records = records;
    }

    @Override
    public String toString() {
        return "ServiceConversationResult{" +
                "orders=" + orders +
                ", pages=" + pages +
                ", current=" + current +
                ", optimizeCountSql=" + optimizeCountSql +
                ", countId=" + countId +
                ", size=" + size +
                ", hitCount=" + hitCount +
                ", maxLimit=" + maxLimit +
                ", total=" + total +
                ", searchCount=" + searchCount +
                ", records=" + records +
                '}';
    }
}
