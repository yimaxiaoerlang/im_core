package com.yimaxiaoerlang.im_core.model.message;

/**
 * 文本消息
 */
public class YMTextMessage extends YMMessageContent {
    private String content;

    public static YMTextMessage create(String text) {
        YMTextMessage message = new YMTextMessage();
        message.setContent(text);
        return message;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }


}
