package com.yimaxiaoerlang.im_core.model;

import com.yimaxiaoerlang.im_core.model.conversation.YMConversation;

import java.io.Serializable;

public class YMContacts implements Serializable {
    private String character;
    private String noteName;
    private String tid;
    private String userAvatar;
    private String username;
    private YMConversation.ConversationType conversationType;

    public String getCharacter() {
        return character;
    }

    public void setCharacter(String character) {
        this.character = character;
    }

    public String getNoteName() {
        return noteName;
    }

    public void setNoteName(String noteName) {
        this.noteName = noteName;
    }

    public String getTid() {
        return tid;
    }

    public void setTid(String tid) {
        this.tid = tid;
    }

    public String getUserAvatar() {
        return userAvatar;
    }

    public void setUserAvatar(String userAvatar) {
        this.userAvatar = userAvatar;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public YMConversation.ConversationType getConversationType() {
        return conversationType;
    }

    public void setConversationType(YMConversation.ConversationType conversationType) {
        this.conversationType = conversationType;
    }
}
