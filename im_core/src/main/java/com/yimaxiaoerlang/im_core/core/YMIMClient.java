package com.yimaxiaoerlang.im_core.core;

import android.content.Context;
import android.text.TextUtils;

import com.yimaxiaoerlang.im_core.core.other.YMResultCallback;
import com.yimaxiaoerlang.ym_base.YMConfig;
import com.yimaxiaoerlang.ym_base.YMLog;
import com.yimaxiaoerlang.http.OkHttpClientHelper;
import com.yimaxiaoerlang.im_core.core.callback.MessageCallback;
import com.yimaxiaoerlang.im_core.core.other.YMConnectCallback;
import com.yimaxiaoerlang.im_core.core.other.YMConnectionStatus;
import com.yimaxiaoerlang.im_core.core.other.YMConnectionStatusListener;
import com.yimaxiaoerlang.im_core.core.other.YMReceiveMessageListener;
import com.yimaxiaoerlang.im_core.manager.YMChatManager;
import com.yimaxiaoerlang.im_core.manager.YMFriendManager;
import com.yimaxiaoerlang.im_core.manager.YMGroupManager;
import com.yimaxiaoerlang.im_core.manager.YMRoomManager;
import com.yimaxiaoerlang.im_core.manager.YMServiceManager;
import com.yimaxiaoerlang.im_core.model.message.YMMessage;

import okhttp3.Interceptor;

public class YMIMClient {
    public static final String SDK_VERSION = "1.0.7";

    public static YMIMClient getInstance() {
        return YMIMClient.SingletonHolder.sInstance;
    }
    private String nowToken = "";
    private YMConfig ymimConfig;

    private static Context appContext;
    private static class SingletonHolder {
        static YMIMClient sInstance = new YMIMClient();

        private SingletonHolder() {
        }
    }

    public void setAppContext(Context context) {
        appContext = context;
    }

    public static Context getAppContext() {
        return appContext;
    }

    /**
     * SDK版本号
     * @return SDK版本号
     */
    public String version() {
        return SDK_VERSION;
    }

    public void initSDKWithConfig(YMConfig config) {
        ymimConfig = config;
        if (config == null) {
            YMLog.e("SDK初始化未配置YMIMConfig，这将导致IM功能无法正常使用");
        }
        if (TextUtils.isEmpty(ymimConfig.getAppkey())) {
            YMLog.e("SDK初始化未配置Appkey，这将导致IM功能无法正常使用");
        }
        if (TextUtils.isEmpty(ymimConfig.getAppsecret())) {
            YMLog.e("SDK初始化未配置Appsecret，这将导致IM功能无法正常使用");
        }
    }

    public YMConfig getConfig() {
        return ymimConfig;
    }

    /**
     * 设置socket地址和接口请求的地址
     *
     * @param socketUrl
     * @param baseUrl
     */
    public YMIMClient configAddress(String socketUrl, String baseUrl) {
        YMServiceManager.getInstance().configAddress(baseUrl, socketUrl);
        return this;
    }

    public YMIMClient addInterceptor(Interceptor interceptor) {
        OkHttpClientHelper.getInstance().addInterceptor(interceptor);
        return this;
    }


    /**
     * 通过token 进行socket链接
     *
     * @param token
     */
    public void loginWithToken(String token, String userId, YMConnectCallback connectCallback) {
        YMServiceManager.connect(token, userId, connectCallback);
    }


    /**
     * 获取token
     *
     * @return
     */
    public String getToken() {
        return YMServiceManager.getToken();
    }

    /**
     * 退出登录
     */
    public void logout() {
        YMServiceManager.logout();
    }

    /**
     * 断开链接
     */
    public void disconnect() {
        YMServiceManager.logout();
    }

    /**
     * 设置链接状态的接听
     *
     * @param connectionStatusListener
     */
    public void setConnectionStatusListener(final YMConnectionStatusListener connectionStatusListener) {
        YMServiceManager.setConnectionStatusListener(connectionStatusListener);
    }

    /**
     * 移除
     */
    public void removeConnectionStatusListener() {
        YMServiceManager.removeConnectionStatusListener();
    }


    /**
     * 获取当前的链接状态
     *
     * @return
     */
    public YMConnectionStatus getCurrentConnectionStatus() {
        return YMServiceManager.getCurrentConnectionStatus();
    }

    public static YMChatManager chatManager() {
        return YMChatManager.getInstance();
    }

    public static YMFriendManager friendManager() {
        return YMFriendManager.getInstance();
    }

    public static YMGroupManager groupManager() {
        return YMGroupManager.getInstance();
    }

    public static YMRoomManager roomManager() {
        return YMRoomManager.getInstance();
    }

    /**
     * 添加收到消息的监听
     *
     * @param listener
     */
    public void addReceiveMessageListener(final YMReceiveMessageListener listener) {
        YMServiceManager.addReceiveMessageListener(listener);
    }

    /**
     * 移除收到消息的监听
     *
     * @param listener
     */
    public void removeReceiveMessageListener(final YMReceiveMessageListener listener) {
        YMServiceManager.removeReceiveMessageListener(listener);
    }

    /**
     * 阅读消息
     *
     * @param message
     */
    public void readMessage(YMMessage message) {
        YMServiceManager.readMessage(message);
    }

    /**
     * 设置消息监听。所有soceket消息
     */
    public void setMsgListener(MessageCallback callback) {
        YMServiceManager.setRTCRecive(callback);
    }

    /**
     * 绑定个推clientId
     * @param userId
     * @param clientId
     */
    public void bindGTPushClientId(String userId, String clientId) {
        YMServiceManager.bindGTPushClientId(userId, clientId, new YMResultCallback<Object>() {
            @Override
            public void onSuccess(Object var1) {

            }

            @Override
            public void onError(int errorCode) {

            }
        });
    }

}

