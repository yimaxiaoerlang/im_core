package com.yimaxiaoerlang.im_core.core.other;

/**
 * 链接状态的监听
 */
public interface YMConnectionStatusListener {
    void onChanged(YMConnectionStatus status);
}
