package com.yimaxiaoerlang.im_core.core.other;


public abstract class YMResultCallback<T> {
    public YMResultCallback() {
    }

    public abstract void onSuccess(T var1);

    public abstract void onError(int errorCode);

    public void onCallback(final T t) {
        YMResultCallback.this.onSuccess(t);
    }

    public void onFail(int errorCode) {
        YMResultCallback.this.onError(errorCode);
    }

}
