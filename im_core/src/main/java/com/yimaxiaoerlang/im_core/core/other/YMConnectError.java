package com.yimaxiaoerlang.im_core.core.other;

public enum YMConnectError {
    UNSEETINGURL(1, "not config socket address and api address"),
    AUTHENTICATIONERROR(2,"authentication error"),
    TOKENEMTY(3,"token is isEmpty"),
    USERIDEMTYP(4,"userId is isEmpty");
    private int code;
    private String msg;

    private YMConnectError(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }
    public int getValue() {
        return this.code;
    }

    public String getMessage() {
        return this.msg;
    }
}
