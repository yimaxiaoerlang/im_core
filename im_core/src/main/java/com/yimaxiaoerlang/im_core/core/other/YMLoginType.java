package com.yimaxiaoerlang.im_core.core.other;

public enum YMLoginType {
    // 登录服务类型：0即时通信 1音视频通信 2信令 3直播
    IM(0),
    CALL(1),
    SIGNALING(2),
    LIVE(3);

    private final int type;

    public int getType() {
        return type;
    }

    YMLoginType(int type) {
        this.type = type;
    }

}
