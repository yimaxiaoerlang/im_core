package com.yimaxiaoerlang.im_core.core.other;


/**
 * token 链接监听
 */
public abstract class YMConnectCallback {
    public YMConnectCallback() {
    }

    //链接成功
    public abstract void onSuccess(Object obj);

    //发生错误
    public abstract void onError(YMConnectError error);

}
