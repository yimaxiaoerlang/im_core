package com.yimaxiaoerlang.im_core.core.other;


/**
 * 链接的状态
 */
public enum YMConnectionStatus {
    NETWORK_UNAVAILABLE(-1, "Network is unavailable."),
    CONNECTED(0, "Connect Success."),
    CONNECTING(1, "Connecting"),
    UNCONNECTED(2, "UNCONNECTED"),
    KICKED_OFFLINE_BY_OTHER_CLIENT(3, "Login on the other device, and be kicked offline."),
    TOKEN_INCORRECT(4, "Token incorrect."),
    CONN_USER_BLOCKED(6, "User blocked by admin"),
    SIGN_OUT(12, "user sign out"),
    SUSPEND(13, "SUSPEND"),
    TIMEOUT(14, "TIMEOUT"),
    LOGIN_OTHER(15, "login on other phone"),
    UNKOWN(16, "unkown");
    private int code;
    private String msg;

    private YMConnectionStatus(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }
    public int getValue() {
        return this.code;
    }

    public String getMessage() {
        return this.msg;
    }
}
