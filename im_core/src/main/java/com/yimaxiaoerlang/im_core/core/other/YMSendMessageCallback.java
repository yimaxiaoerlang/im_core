package com.yimaxiaoerlang.im_core.core.other;

import com.yimaxiaoerlang.im_core.model.message.YMMessage;

public interface YMSendMessageCallback {
    void onSuccess(YMMessage message);

    void onError(YMMessage message, int code);
}
