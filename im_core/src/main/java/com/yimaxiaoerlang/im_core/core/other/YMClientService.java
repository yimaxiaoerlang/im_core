package com.yimaxiaoerlang.im_core.core.other;

import com.yimaxiaoerlang.http.ResultBean;
import com.yimaxiaoerlang.im_core.model.YMApplyUserListEntity;
import com.yimaxiaoerlang.im_core.model.YMChatroomEntity;
import com.yimaxiaoerlang.im_core.model.YMContacts;
import com.yimaxiaoerlang.im_core.model.YMGroupEntity;
import com.yimaxiaoerlang.im_core.model.YMLoginResult;
import com.yimaxiaoerlang.im_core.model.YMRoomEntity;
import com.yimaxiaoerlang.im_core.model.YMServiceConversation;
import com.yimaxiaoerlang.im_core.model.YMServiceConversationResult;
import com.yimaxiaoerlang.im_core.model.YMServiceMessageInfo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.reactivex.Observable;
import okhttp3.MultipartBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

public interface YMClientService {
    @POST("/chat/login")
    Observable<ResultBean<YMLoginResult>> login(@Body HashMap<String, String> map);

    @POST("/chat/login/gt")
    Observable<ResultBean<Object>> bindPushClientId(@Body HashMap<String, String> map);

    //群组列表，会话列表
    @POST("chat/group/list")
    Observable<ResultBean<YMServiceConversationResult<YMServiceConversation>>> conversationList(@Body HashMap<String, Object> map);

    //通讯录列表
    @POST("chat/user/friend/list")
    Observable<ResultBean<ArrayList<YMContacts>>> contactsList(@Body HashMap<String, Object> map);

    //单聊，需要创建的群聊
    @POST("chat/group/single")
    Observable<ResultBean<YMServiceConversation>> createPrivateChar(@Body HashMap<String, Object> map);

    //群组详情
    @POST("chat/group/detail")
    Observable<ResultBean<YMServiceConversation>> groupDetail(@Body HashMap<String, Object> map);

    //获取消息
    @POST("chat/message/list")
    Observable<ResultBean<YMServiceConversationResult<YMServiceMessageInfo>>> messageList(@Body HashMap<String, Object> map);

    //发送消息
    @POST("chat/message/send")
    Observable<ResultBean<Object>> sendMessage(@Body HashMap<String, Object> map);

    //删除会话
    @POST("chat/group/del")
    Observable<ResultBean<Object>> delMessage(@Body HashMap<String, Object> map);

    //清空会话
    @POST("chat/message/clear")
    Observable<ResultBean<Object>> clearMessage(@Body HashMap<String, Object> map);

    //添加好友
    @POST("chat/user/friend/add")
    Observable<ResultBean<Object>> addFriend(@Body HashMap<String, Object> map);

    //消息置顶(群组置顶)
    @POST("chat/group/top")
    Observable<ResultBean<Object>> setConversationTop(@Body HashMap<String, Object> map);

    //设置消息免打扰
    @POST("chat/group/disturb")
    Observable<ResultBean<Object>> setConversationDisturb(@Body HashMap<String, Object> map);

    //创建群聊
    @POST("chat/group/create")
    Observable<ResultBean<YMGroupEntity>> createGroup(@Body HashMap<String, Object> map);

    //创建聊天室
    @POST("chat/room/create")
    Observable<ResultBean<YMRoomEntity>> createRoom(@Body HashMap<String, Object> map);

    //加入聊天室
    @POST("chat/room/join")
    Observable<ResultBean<Object>> joinRoom(@Body HashMap<String, Object> map);

    //解散聊天室
    @POST("chat/room/dismiss")
    Observable<ResultBean<Object>> dismissRoom(@Body HashMap<String, Object> map);

    //聊天室禁言用户
    @POST("chat/room/ban")
    Observable<ResultBean<Object>> banRoomUser(@Body HashMap<String, Object> map);

    //聊天室解除禁言用户
    @POST("chat/room/open")
    Observable<ResultBean<Object>> openRoomUser(@Body HashMap<String, Object> map);

    //聊天室邀请用户
    @POST("chat/room/invite")
    Observable<ResultBean<Object>> inviteRoom(@Body HashMap<String, Object> map);

    //请离聊天室
    @POST("chat/room/kick")
    Observable<ResultBean<Object>> kickRoom(@Body HashMap<String, Object> map);

    //离开聊天室
    @POST("chat/room/leave")
    Observable<ResultBean<Object>> leaveRoom(@Body HashMap<String, Object> map);

    //离开群聊
    @POST("chat/group/leave")
    Observable<ResultBean<Object>> leaveGroup(@Body HashMap<String, Object> map);

    //邀请好友加入群组
    @POST("chat/group/invite")
    Observable<ResultBean<Object>> groupInvite(@Body HashMap<String, Object> map);

    //群组踢人
    @POST("chat/group/kick")
    Observable<ResultBean<Object>> groupKick(@Body HashMap<String, Object> map);

    //获取群详情
    @POST("chat/group/detail")
    Observable<ResultBean<YMGroupEntity>> getGroupDetail(@Body HashMap<String, Object> map);

    //修改群名称
    @POST("chat/group/name")
    Observable<ResultBean<Object>> changeGroupName(@Body HashMap<String, Object> map);

    //处理好友申请
    @POST("chat/user/friend/handle")
    Observable<ResultBean<Object>> handleFriend(@Body HashMap<String, Object> map);

    //申请好友列表
    @POST("chat/user/friend/list/apply")
    Observable<ResultBean<YMApplyUserListEntity>> applyUserList(@Body HashMap<String, Object> map);

    // 获取好友详情
    @GET("chat/user/friend/detail")
    Observable<ResultBean<YMContacts>> getFriendDetail(@Query("tid") String userId);

    /**
     * 搜索用户
     * @param keyword 搜索关键字
     * @return 用户
     */
    @GET("chat/user/friend/search")
    Observable<ResultBean<List<YMContacts>>> searchUser(@Query("queryStr") String keyword);

    /**
     * 更改好友备注
     * @param map
     * @return
     */
    @POST("chat/user/friend/note")
    Observable<ResultBean<Object>> changeFriendNote(@Body HashMap<String, Object> map);

    /**
     * 解散群聊
     * @param map
     * @return
     */
    @POST("chat/group/dismiss")
    Observable<ResultBean<Object>> dismissGroupWithGroupId(@Body HashMap<String, Object> map);

    /**
     * 查询全部聊天室
     * @param map
     * @return
     */
    @POST("chat/room/all")
    Observable<ResultBean<List<YMChatroomEntity>>> getRoomList(@Body HashMap<String, Object> map);

    /**
     * 搜索查询聊天室
     * @param map
     * @return
     */
    @POST("chat/room/search")
    Observable<ResultBean<List<YMChatroomEntity>>> searchRoomList(@Body HashMap<String, Object> map);

    //上传文件
    @POST("file/upload")
    @Multipart
    Observable<ResultBean<String>> upLoadFile(@Part MultipartBody.Part requestBody, @QueryMap HashMap<String, Object> map);

    @POST("file/upload")
    @Multipart
    Observable<ResultBean<String>> upLoadFiles(@Part List<MultipartBody.Part> parts);

}
