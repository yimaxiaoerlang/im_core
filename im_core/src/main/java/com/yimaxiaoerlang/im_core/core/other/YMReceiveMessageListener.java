package com.yimaxiaoerlang.im_core.core.other;

import com.yimaxiaoerlang.im_core.model.message.YMMessage;

public interface YMReceiveMessageListener {
    void onReceiveMessage(YMMessage message);
}


