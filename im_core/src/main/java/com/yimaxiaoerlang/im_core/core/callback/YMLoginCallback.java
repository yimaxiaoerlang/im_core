package com.yimaxiaoerlang.im_core.core.callback;

public interface YMLoginCallback {
    void loginSuccess();

    void loginError();
}
