package com.yimaxiaoerlang.im_core.manager;

import com.yimaxiaoerlang.im_core.core.other.YMResultCallback;
import com.yimaxiaoerlang.im_core.model.YMChatroomEntity;
import com.yimaxiaoerlang.im_core.model.YMRoomEntity;

import java.util.List;

public abstract class YMRoomManager {
    public static YMRoomManager getInstance() {
        return YMRoomManagerImpl.getInstance();
    }

    /**
     * 创建聊天室
     * @param name
     * @param portrait
     * @param uidList
     */
    public abstract void createRoomWithName(String name, String portrait, List<String> uidList, final YMResultCallback<YMRoomEntity> callback);

    /**
     * 创建聊天室
     * @param name
     * @param portrait
     * @param uids
     */
    public abstract void createRoomWithName(String name, String portrait, String  uids, final YMResultCallback<YMRoomEntity> callback);

    /**
     * 加入聊天室
     * @param groupId
     * @param callback
     */
    public abstract void joinRoom(String groupId, final YMResultCallback<Object> callback);


    /**
     * 解散聊天室
     * @param groupId
     * @param callback
     */
    public abstract void dismissRoom(String groupId, final YMResultCallback<Object> callback);

    /**
     * 禁言用户
     * @param groupId
     * @param userId
     * @param callback
     */
    public abstract void banRoomUser(String groupId, String userId, final YMResultCallback<Object> callback);

    /**
     * 解除禁言用户
     * @param groupId
     * @param userId
     * @param callback
     */
    public abstract void openRoomUser(String groupId, String userId, final YMResultCallback<Object> callback);

    /**
     * 聊天室邀请用户
     * @param groupId
     * @param uidList
     * @param callback
     */
    public abstract void inviteRoom(String groupId, List<String> uidList, final YMResultCallback<Object> callback);

    /**
     * 聊天室请离用户
     * @param groupId
     * @param uidList
     * @param callback
     */
    public abstract void kickRoom(String groupId, List<String> uidList, final YMResultCallback<Object> callback);

    /**
     * 离开聊天室
     * @param groupId
     * @param callback
     */
    public abstract void leaveRoom(String groupId, final YMResultCallback<Object> callback);

    /**
     * 查询聊天室列表
     * @param keyword
     * @param callback
     */
    public abstract void searchRoomList(String keyword, final YMResultCallback<List<YMChatroomEntity>> callback);
}
