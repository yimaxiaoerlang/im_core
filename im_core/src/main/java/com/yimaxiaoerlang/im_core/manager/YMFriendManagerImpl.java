package com.yimaxiaoerlang.im_core.manager;

import com.yimaxiaoerlang.im_core.core.other.YMResultCallback;
import com.yimaxiaoerlang.im_core.model.YMApplyUserListEntity;
import com.yimaxiaoerlang.im_core.model.YMContacts;
import com.yimaxiaoerlang.im_core.model.conversation.YMConversation;

import java.util.ArrayList;
import java.util.List;

public class YMFriendManagerImpl extends YMFriendManager {

    private static class YMFriendManagerImplHolder {
        private static final YMFriendManagerImpl ymFriendManagerImpl = new YMFriendManagerImpl();
    }

    public static YMFriendManagerImpl getInstance() {
        return YMFriendManagerImpl.YMFriendManagerImplHolder.ymFriendManagerImpl;
    }

    private YMFriendManagerImpl() {

    }

    @Override
    public void getFriendsListWithCompletion(YMResultCallback<List<YMContacts>> callback) {
        List<YMConversation.ConversationType> typeList = new ArrayList<>();
        typeList.add(YMConversation.ConversationType.PRIVATE);
        typeList.add(YMConversation.ConversationType.GROUP);
        typeList.add(YMConversation.ConversationType.CHATROOM);
        YMServiceManager.getContacts(typeList, 1, 1000, "", callback);
    }

    @Override
    public void searchFriendsWithKeyword(String keyword, final YMResultCallback<List<YMContacts>> callback) {
        YMServiceManager.searchUser(keyword, callback);
    }

    @Override
    public void getFriendsApplyListWithPage(int page, int pageSize, YMResultCallback<YMApplyUserListEntity> callback) {
        YMServiceManager.applyUserList(page, pageSize, callback);
    }

    @Override
    public void applyAddFriendWithTargetId(String targetUserId, String applyMessage, YMResultCallback<Boolean> callback) {
        YMServiceManager.addFriend(targetUserId, applyMessage, callback);
    }

    @Override
    public void handleFriendApplyWithApplyId(String applyId, boolean isAgree, YMResultCallback<Boolean> callback) {
        YMServiceManager.handleFriend(isAgree ? 2 : 3, applyId, callback);
    }

    @Override
    public void getFriendDetailWithFriendId(String friendId, YMResultCallback<YMContacts> callback) {
        YMServiceManager.getFriendDetail(friendId, callback);
    }

    @Override
    public void addFriendNoteWithFriendId(String friendId, String note, YMResultCallback<Boolean> callback) {
        YMServiceManager.changeFriendNote(friendId, note, callback);
    }

    @Override
    public void deleteFriendWithFriendId(String friendId, YMResultCallback<Boolean> callback) {

    }
}
