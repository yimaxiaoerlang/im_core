package com.yimaxiaoerlang.im_core.manager;

import com.yimaxiaoerlang.im_core.core.other.YMResultCallback;
import com.yimaxiaoerlang.im_core.model.YMGroupEntity;
import com.yimaxiaoerlang.im_core.model.conversation.YMConversation;
import com.yimaxiaoerlang.im_core.model.message.YMCustomMessage;
import com.yimaxiaoerlang.im_core.model.message.YMImageMessage;
import com.yimaxiaoerlang.im_core.model.message.YMMessage;
import com.yimaxiaoerlang.im_core.model.message.YMTextMessage;
import com.yimaxiaoerlang.im_core.model.message.YMVideoMessage;

import java.util.List;

public class YMChatManagerImpl extends YMChatManager {

    private static class YMChatManagerImplHolder {
        private static final YMChatManagerImpl ymChatManagerImpl = new YMChatManagerImpl();
    }

    public static YMChatManagerImpl getInstance() {
        return YMChatManagerImplHolder.ymChatManagerImpl;
    }

    private YMChatManagerImpl() {

    }

    @Override
    public void sendMessage(YMMessage message, YMResultCallback<Boolean> callback) {
        if (message.getContent() instanceof YMTextMessage) {
            YMServiceManager.sendMessage(message, callback);
        } else {
            if (message.getContent() instanceof YMImageMessage) {
                YMImageMessage imageMessage = (YMImageMessage) message.getContent();
                if (imageMessage.getUrl() != null && imageMessage.getUrl().startsWith("http")) {
                    YMServiceManager.sendMessage(message, callback);
                    return;
                }
            } else if (message.getContent() instanceof YMVideoMessage) {
                YMVideoMessage videoMessage = (YMVideoMessage) message.getContent();
                if (videoMessage.getUrl() != null && videoMessage.getUrl().startsWith("http")) {
                    YMServiceManager.sendMessage(message, callback);
                    return;
                }
            } else if (message.getContent() instanceof YMCustomMessage) {
                YMServiceManager.sendMessage(message, callback);
            }
            YMServiceManager.sendFileMessage(message, callback);

        }
    }

    @Override
    public void updateUserInfo() {

    }

    @Override
    public void joinPrivateChatWithTargetId(String targetId, int label, final YMResultCallback<YMConversation> callback) {
        YMServiceManager.createPrivateChat(label + "", targetId, callback);
    }

    @Override
    public void joinPrivateChatWithTargetId(String targetId, final YMResultCallback<YMConversation> callback) {
        YMServiceManager.createPrivateChat("0", targetId, callback);
    }

    @Override
    public void getConversationList(List<YMConversation.ConversationType> conversationTypeList, int pageIndex, int pageSize, int label, final YMResultCallback<List<YMConversation>> callback) {
        YMServiceManager.getConversationList(conversationTypeList, pageIndex, pageSize, label, callback);
    }

    @Override
    public void getConversationList(List<YMConversation.ConversationType> conversationTypeList, int pageIndex, int pageSize, final YMResultCallback<List<YMConversation>> callback) {
        YMServiceManager.getConversationList(conversationTypeList, pageIndex, pageSize, null, callback);
    }

    @Override
    public void removeConversationWithTargetId(String groupId, YMResultCallback<Boolean> callback) {
        YMServiceManager.delMessage(groupId, callback);
    }

    @Override
    public void getConversationWithTargetId(String groupId, final YMResultCallback<YMConversation> callback) {
        YMServiceManager.conversationDel(groupId, callback);
    }

    @Override
    public void getConversationInfoWithTargetId(String groupId, YMResultCallback<YMGroupEntity> callback) {
        YMServiceManager.getGroupDetail(groupId, callback);
    }

    @Override
    public void getHistoryMessageWithTargetId(String groupId, int pageIndex, int pageSize, final YMResultCallback<List<YMMessage>> callback) {
        YMServiceManager.messageList(groupId, pageIndex, pageSize, "", callback);
    }

    @Override
    public void topConversationWithTargetId(String groupId, YMResultCallback<Boolean> callback) {
        YMServiceManager.setConversationTop(groupId, callback);
    }

    @Override
    public void doNotDisturbConversationWithTargetId(String groupId, YMResultCallback<Boolean> callback) {
        YMServiceManager.setConversationDisturb(groupId, callback);
    }

    @Override
    public void unreadMessageCountWithCompletion() {

    }

    @Override
    public void searchUserListWithKeyword(String keyword) {

    }

    @Override
    public void clearMessageWithTargetId(String groupId, YMResultCallback<Boolean> callback) {
        YMServiceManager.clearMessage(groupId, callback);
    }
}
