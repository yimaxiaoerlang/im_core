package com.yimaxiaoerlang.im_core.manager;

import com.yimaxiaoerlang.im_core.core.other.YMResultCallback;
import com.yimaxiaoerlang.im_core.model.YMGroupEntity;
import com.yimaxiaoerlang.im_core.model.conversation.YMConversation;

import java.util.ArrayList;
import java.util.List;

public class YMGroupManagerImpl extends YMGroupManager{

    private static class YMGroupManagerImplHolder {
        private static final YMGroupManagerImpl ymGroupManagerImpl = new YMGroupManagerImpl();
    }

    public static YMGroupManagerImpl getInstance() {
        return YMGroupManagerImpl.YMGroupManagerImplHolder.ymGroupManagerImpl;
    }

    private YMGroupManagerImpl() {

    }

    @Override
    public void createGroupWithName(String name, String portrait, List<String> uidsList, int label, YMResultCallback<YMGroupEntity> callback) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < uidsList.size(); i++) {
            if (i > 0) {
                sb.append(",");
            }
            sb.append(uidsList.get(i));
        }
        YMServiceManager.createGroup(portrait, name, sb.toString(), callback);
    }

    @Override
    public void createGroupWithName(String name, String portrait, List<String> uidsList, YMResultCallback<YMGroupEntity> callback) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < uidsList.size(); i++) {
            if (i > 0) {
                sb.append(",");
            }
            sb.append(uidsList.get(i));
        }
        YMServiceManager.createGroup(portrait, name, sb.toString(), callback);
    }

    @Override
    public void getGroupListWithCount(int count, int label, final YMResultCallback<List<YMConversation>> callback) {
        ArrayList<YMConversation.ConversationType> arrayList = new ArrayList<>();
        arrayList.add(YMConversation.ConversationType.GROUP);
        YMServiceManager.getConversationList(arrayList, 1, count == 0 ? 999 : count, label, callback);
    }

    @Override
    public void getGroupListWithPage(int page, int pageSize, YMResultCallback<List<YMConversation>> callback) {
        ArrayList<YMConversation.ConversationType> arrayList = new ArrayList<>();
        arrayList.add(YMConversation.ConversationType.GROUP);
        YMServiceManager.getConversationList(arrayList, page, pageSize, null, callback);
    }

    @Override
    public void dismissGroupWithGroupId(String groupId, YMResultCallback<Boolean> callback) {
        YMServiceManager.dismissGroupWithGroupId(groupId, callback);
    }

    @Override
    public void inviteIntoGroupWithGroupId(String groupId, List<String> uidsList, YMResultCallback<Boolean> callback) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < uidsList.size(); i++) {
            if (i > 0) {
                sb.append(",");
            }
            sb.append(uidsList.get(i));
        }
        YMServiceManager.groupInvite(groupId, sb.toString(), callback);
    }

    @Override
    public void kickGroupWithGroupId(String groupId, List<String> uidsList, YMResultCallback<Boolean> callback) {
        YMServiceManager.groupKick(groupId, uidsList, callback);
    }

    @Override
    public void leaveGroupWithGroupId(String groupId, YMResultCallback<Boolean> callback) {
        YMServiceManager.leaveGroup(groupId, callback);
    }

    @Override
    public void updateGroupNameWithGroupId(String groupId, String groupName, YMResultCallback<Boolean> callback) {
        YMServiceManager.changeGroupName(groupId, groupName, callback);
    }

    @Override
    public void getGroupDetail(String groupId, YMResultCallback<YMGroupEntity> callback) {
        YMServiceManager.getGroupDetail(groupId, callback);
    }
}
