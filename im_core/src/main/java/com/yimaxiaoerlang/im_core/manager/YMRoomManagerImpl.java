package com.yimaxiaoerlang.im_core.manager;

import com.yimaxiaoerlang.im_core.core.other.YMResultCallback;
import com.yimaxiaoerlang.im_core.model.YMChatroomEntity;
import com.yimaxiaoerlang.im_core.model.YMRoomEntity;

import java.util.List;

public class YMRoomManagerImpl extends YMRoomManager {
    private static class YMRoomManagerImplHolder {
        private static final YMRoomManagerImpl ymRoomManagerImpl = new YMRoomManagerImpl();
    }

    public static YMRoomManagerImpl getInstance() {
        return YMRoomManagerImpl.YMRoomManagerImplHolder.ymRoomManagerImpl;
    }

    private YMRoomManagerImpl() {

    }

    @Override
    public void createRoomWithName(String name, String portrait, List<String> uidList, YMResultCallback<YMRoomEntity> callback) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < uidList.size(); i++) {
            if (i > 0) {
                sb.append(",");
            }
            sb.append(uidList.get(i));
        }
        YMServiceManager.createRoom(portrait, name, sb.toString(), callback);
    }

    @Override
    public void createRoomWithName(String name, String portrait, String uids, YMResultCallback<YMRoomEntity> callback) {
        YMServiceManager.createRoom(portrait, name, uids, callback);
    }

    @Override
    public void joinRoom(String groupId, YMResultCallback<Object> callback) {
        YMServiceManager.joinRoom(groupId, callback);
    }

    @Override
    public void dismissRoom(String groupId, YMResultCallback<Object> callback) {
        YMServiceManager.dismissRoom(groupId, callback);
    }

    @Override
    public void banRoomUser(String groupId, String userId, YMResultCallback<Object> callback) {
        YMServiceManager.banRoomUser(groupId, userId, callback);
    }

    @Override
    public void openRoomUser(String groupId, String userId, YMResultCallback<Object> callback) {
        YMServiceManager.openRoomUser(groupId, userId, callback);
    }

    @Override
    public void inviteRoom(String groupId, List<String> uidList, YMResultCallback<Object> callback) {
        YMServiceManager.inviteRoom(groupId, uidList, callback);
    }

    @Override
    public void kickRoom(String groupId, List<String> uidList, YMResultCallback<Object> callback) {
        YMServiceManager.kickRoom(groupId, uidList, callback);
    }

    @Override
    public void leaveRoom(String groupId, YMResultCallback<Object> callback) {
        YMServiceManager.leaveRoom(groupId, callback);
    }

    @Override
    public void searchRoomList(String keyword, YMResultCallback<List<YMChatroomEntity>> callback) {
        YMServiceManager.searchRoomList(keyword, callback);
    }
}
