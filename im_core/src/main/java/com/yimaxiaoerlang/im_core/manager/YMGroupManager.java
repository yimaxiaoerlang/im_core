package com.yimaxiaoerlang.im_core.manager;

import com.yimaxiaoerlang.im_core.core.other.YMResultCallback;
import com.yimaxiaoerlang.im_core.model.YMGroupEntity;
import com.yimaxiaoerlang.im_core.model.conversation.YMConversation;

import java.util.List;

public abstract class YMGroupManager {

    public static YMGroupManager getInstance() {
        return YMGroupManagerImpl.getInstance();
    }
    
    /**
     * 创建群组
     * @param name 群组名称
     * @param portrait 群组图像
     * @param uidList 加入群组的用户列表
     * @param label 分组标签，可以用来分组会话
     */
    public abstract void createGroupWithName(String name, String portrait, List<String> uidList, int label, final YMResultCallback<YMGroupEntity> callback);

    /**
     * 创建群组， 不分组会话
     * @param name
     * @param portrait
     * @param uidList
     */
    public abstract void createGroupWithName(String name, String portrait, List<String> uidList, final YMResultCallback<YMGroupEntity> callback);

    /**
     * 获取群组列表
     * @param count 显示数据 0表示不限
     * @param label 分组标签：区分一组群组列表
     */
    public abstract void getGroupListWithCount(int count, int label, final YMResultCallback<List<YMConversation>> callback);

    /**
     * 获取群组列表
     * @param page 页数
     * @param pageSize 一页数量
     */
    public abstract void getGroupListWithPage(int page, int pageSize, final YMResultCallback<List<YMConversation>> callback);

    /**
     * 解散群组
     * @param groupId 群组id
     */
    public abstract void dismissGroupWithGroupId(String groupId, final YMResultCallback<Boolean> callback);

    /**
     * 邀请用户进群
     * @param groupId 群组Id
     * @param uidsList 用户唯一身份数组
     */
    public abstract void inviteIntoGroupWithGroupId(String groupId, List<String> uidsList, final YMResultCallback<Boolean> callback);

    /**
     * 请离群组
     * @param groupId 群组Id
     * @param uidsList 用户唯一身份数组
     */
    public abstract void kickGroupWithGroupId(String groupId, List<String> uidsList, final YMResultCallback<Boolean> callback);

    /**
     * 用户离开群聊
     * @param groupId 群组Id
     */
    public abstract void leaveGroupWithGroupId(String groupId, final YMResultCallback<Boolean> callback);

    /**
     * 修改群组名称
     * @param groupId 群组Id
     * @param groupName 群组名称
     */
    public abstract void updateGroupNameWithGroupId(String groupId, String groupName, YMResultCallback<Boolean> callback);

    /**
     * 获取群组详情
     * @param groupId
     * @param callback
     */
    public abstract void getGroupDetail(String groupId, final YMResultCallback<YMGroupEntity> callback);
}
