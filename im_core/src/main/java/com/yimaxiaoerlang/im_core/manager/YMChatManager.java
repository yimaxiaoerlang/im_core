package com.yimaxiaoerlang.im_core.manager;

import com.yimaxiaoerlang.im_core.core.other.YMResultCallback;
import com.yimaxiaoerlang.im_core.model.YMGroupEntity;
import com.yimaxiaoerlang.im_core.model.conversation.YMConversation;
import com.yimaxiaoerlang.im_core.model.message.YMMessage;

import java.util.List;

public abstract class YMChatManager {
    public YMChatManager() {

    }

    public static YMChatManager getInstance() {
        return YMChatManagerImpl.getInstance();
    }

    /**
     * 发送消息
     */
    public abstract void sendMessage(YMMessage message, YMResultCallback<Boolean> callback);

    /**
     * 用户信息变更，更新用户持久化信息
     */
    public abstract void updateUserInfo();

    /**
     * 加入单聊
     */
    public abstract void joinPrivateChatWithTargetId(String targetId, int label, final YMResultCallback<YMConversation> callback);

    /**
     * 加入单聊
     * 不区分label 会话标签
     *
     * @param targetId
     */
    public abstract void joinPrivateChatWithTargetId(String targetId, final YMResultCallback<YMConversation> callback);

    /**
     * 获取会话列表
     *
     * @param conversationTypeList
     * @param pageIndex
     * @param label
     */
    public abstract void getConversationList(List<YMConversation.ConversationType> conversationTypeList, int pageIndex, int pageSize, int label, final YMResultCallback<List<YMConversation>> callback);

    /**
     * 获取会话列表
     * 不区分label 会话标签
     *
     * @param conversationTypeList
     * @param pageIndex
     */
    public abstract void getConversationList(List<YMConversation.ConversationType> conversationTypeList, int pageIndex, int pageSize, final YMResultCallback<List<YMConversation>> callback);

    /**
     * 删除会话
     *
     * @param groupId
     */
    public abstract void removeConversationWithTargetId(String groupId, YMResultCallback<Boolean> callback);

    /**
     * 获取会话详情
     */
    public abstract void getConversationWithTargetId(String groupId, final YMResultCallback<YMConversation> callback);

    /**
     * 获取会话详情
     */
    public abstract void getConversationInfoWithTargetId(String groupId, final YMResultCallback<YMGroupEntity> callback);

    /**
     * 获取某个会话下的消息
     *
     * @param groupId
     * @param pageIndex
     * @param pageSize
     */
    public abstract void getHistoryMessageWithTargetId(String groupId, int pageIndex, int pageSize, final YMResultCallback<List<YMMessage>> callback);

    /**
     * 置顶会话
     *
     * @param groupId
     */
    public abstract void topConversationWithTargetId(String groupId, YMResultCallback<Boolean> callback);

    /**
     * 会话消息免打扰
     *
     * @param groupId
     */
    public abstract void doNotDisturbConversationWithTargetId(String groupId, YMResultCallback<Boolean> callback);

    /**
     * 获取所有未读消息数量
     */
    public abstract void unreadMessageCountWithCompletion();

    /**
     * 搜索用户
     */
    public abstract void searchUserListWithKeyword(String keyword);

    /**
     * 清空聊天消息
     *
     * @param groupId
     */
    public abstract void clearMessageWithTargetId(String groupId, YMResultCallback<Boolean> callback);
}
