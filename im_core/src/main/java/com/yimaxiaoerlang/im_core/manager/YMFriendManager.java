package com.yimaxiaoerlang.im_core.manager;

import com.yimaxiaoerlang.im_core.core.other.YMResultCallback;
import com.yimaxiaoerlang.im_core.model.YMApplyUserListEntity;
import com.yimaxiaoerlang.im_core.model.YMContacts;

import java.util.List;

public abstract class YMFriendManager {

    public static YMFriendManager getInstance() {
        return YMFriendManagerImpl.getInstance();
    }
    
    public YMFriendManager() {
        
    }
    /**
     * 获取好友列表
     */
    public abstract void getFriendsListWithCompletion(YMResultCallback<List<YMContacts>> callback);

    /**
     * 搜索好友
     * @param keyword 搜索关键字
     */
    public abstract void searchFriendsWithKeyword(String keyword, final YMResultCallback<List<YMContacts>> callback);

    /**
     * 获取好友申请列表
     * @param page 页码
     * @param pageSize 每页显示的数量
     */
    public abstract void getFriendsApplyListWithPage(int page, int pageSize, YMResultCallback<YMApplyUserListEntity> callback);

    /**
     * 申请添加好友
     * @param targetUserId
     * @param applyMessage
     */
    public abstract void applyAddFriendWithTargetId(String targetUserId, String applyMessage, YMResultCallback<Boolean> callback);

    /**
     * 处理添加好友申请
     * @param applyId 申请id
     * @param isAgree 是否同意
     */
    public abstract void handleFriendApplyWithApplyId(String applyId, boolean isAgree, YMResultCallback<Boolean> callback);

    /**
     * 获取好友详情
     * @param friendId 好友Id
     */
    public abstract void getFriendDetailWithFriendId(String friendId, final YMResultCallback<YMContacts> callback);

    /**
     * 添加好友备注
     * @param friendId 好友Id
     * @param note 备注
     */
    public abstract void addFriendNoteWithFriendId(String friendId, String note, final YMResultCallback<Boolean> callback);

    /**
     * 删除好友
     * @param friendId 好友Id
     */
    public abstract void deleteFriendWithFriendId(String friendId, final YMResultCallback<Boolean> callback);
}
